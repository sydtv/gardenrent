<?php

class User
{
    // DB-Properties
    private $conn;
    private $table = 'user';

    // User-Properties
    public $id;
    public $name;
    public $email;
    public $password;
    public $token;

    // Constructor
    public function __construct($db)
    {
        $this->conn = $db;
    }

    // Get Single User
    public function read_single()
    {
        // SQL-Query
        $query = 'SELECT name, email FROM ' . $this->table . ' WHERE id = ?';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Bind ID
        $stmt->bindParam(1, $this->id);

        // Execute query
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        // set properties
        $this->name = $row['name'];
        $this->email = $row['email'];
    }

    public function register()
    {
        // SQL-Query
        $query = 'INSERT INTO ' . $this->table . ' 
                  (name, email, password) 
                  VALUES (:name, :email, :password)';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Bind ID
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':password', $this->password);

        // Execute query
        if ($stmt->execute()) {
            return true;
        }

        printf('Error: %s \n', $stmt->error);

        return false;
    }

    public function login()
    {
        $query = 'SELECT * FROM user WHERE email = :email';

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':email', $this->email);

        // Execute query
        if ($stmt->execute()) {
            $array = $stmt->fetchAll();

            if (count($array) == 1) {
                $dbPassword = $array[0]['password'];
                $this->id = $array[0]['id'];

                if (password_verify($this->password, $dbPassword)) {
                    return $this->createToken();
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function createToken()
    {
        $token = $this->generateRandomString(42);

        $query = "INSERT INTO session (User_ID, Token) VALUES (:id, :token);";

        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(':id', $this->id);
        $stmt->bindParam(':token', $token);

        // Execute query
        if ($stmt->execute()) {
            $this->token = $token;
            return true;
        }

        printf('Error: %s \n', $stmt->error);

        return false;
    }

    public function generateRandomString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}