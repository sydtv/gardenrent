<?php

class Advert
{
    // DB-Properties
    private $conn;
    private $table = 'advert';

    // User-Properties
    public $id;
    public $title;
    public $description;
    public $files;
    public $price;
    public $available;
    public $address;
    public $zip;
    public $city;
    public $authorId;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    // Get all adverts
    public function read()
    {
        // SQL-Query
        $query = 'SELECT a.id as advert_id, 
                        a.author as author,
                        a.title,
                        a.description,
                        a.price,
                        a.available,
                        a.address,
                        a.zip,
                        a.city,
                        u.id as user_id,
                        u.name,
                        u.email, 
                        GROUP_CONCAT(f.location) as files
                    FROM ' . $this->table . ' a
                    LEFT JOIN advert_files f ON f.advert_id = a.id
                    LEFT JOIN user u ON author = u.id
                    GROUP BY a.id';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Execute query
        $stmt->execute();

        return $stmt;
    }

    // Get Single Advert
    public function read_single()
    {
        // SQL-Query
        $query = 'SELECT a.id as advert_id, 
                        a.author as author,
                        a.title,
                        a.description,
                        a.price,
                        a.available,
                        a.address,
                        a.zip,
                        a.city,
                        u.id as user_id,
                        u.name,
                        u.email, 
                        GROUP_CONCAT(f.location) as files
                    FROM ' . $this->table . ' a
                    LEFT JOIN advert_files f ON f.advert_id = a.id
                    LEFT JOIN user u ON author = u.id
                    WHERE a.id = ?
                    GROUP BY a.id';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Bind ID
        $stmt->bindParam(1, $this->id);

        // Execute query
        $stmt->execute();

        return $stmt;
    }

    // Create Advert
    public function create()
    {
        // Create query
        $query = 'INSERT INTO ' . $this->table . ' 
                  SET
                    title = :title,
                    description = :description,
                    price = :price,
                    address = :address,
                    zip = :zip,
                    city = :city,
                    author = :author,
                    available = :available';

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Clean data
        $this->title = htmlspecialchars(strip_tags($this->title));
        $this->description = htmlspecialchars(strip_tags($this->description));
        $this->price = htmlspecialchars(strip_tags($this->price));
        $this->address = htmlspecialchars(strip_tags($this->address));
        $this->zip = htmlspecialchars(strip_tags($this->zip));
        $this->city = htmlspecialchars(strip_tags($this->city));

        // Bind data
        $stmt->bindParam(':title', $this->title);
        $stmt->bindParam(':description', $this->description);
        $stmt->bindParam(':price', $this->price);
        $stmt->bindParam(':address', $this->address);
        $stmt->bindParam(':zip', $this->zip);
        $stmt->bindParam(':city', $this->city);
        $stmt->bindParam(':author', $this->authorId);
        $stmt->bindParam(':available', $this->available);

        // Execute query
        if ($stmt->execute()) {
            if ($this->files['name'][0]) {
                $id = $this->conn->lastInsertId();

                $file_locations = [];
                $countfiles = count($this->files['name']);

                // Looping all files
                for($i = 0; $i < $countfiles; $i++){
                    $filename = str_replace(' ', '', $this->files['name'][$i]);
                    $new_name = time() . '_' . $filename;

                    $file_locations[] = '/api/user_upload/' . $new_name;

                    move_uploaded_file($this->files['tmp_name'][$i], '../../user_upload/' . $new_name);
                }

                // Create query
                $query = 'INSERT INTO advert_files (location, advert_id) VALUES ';

                $insertQuery = array();
                $insertData = array();

                foreach ($file_locations as $row) {
                    $insertQuery[] = '(?, ?)';
                    $insertData[] = $row;
                    $insertData[] = $id;
                }

                if (!empty($insertQuery)) {
                    $query .= implode(', ', $insertQuery);
                    $stmt = $this->conn->prepare($query);
                    if ($stmt->execute($insertData)) {
                        return true;
                    }
                }
            } else {
                return true;
            }
        }

        printf('Error: %s \n', $stmt->error);

        return false;
    }

    public function getAdvertItem($row)
    {
        return array(
            'id' => $row['advert_id'],
            'title' => $row['title'],
            'description' => $row['description'],
            'address' => $row['address'],
            'zip' => $row['zip'],
            'city' => $row['city'],
            'available' => !!$row['available'],
            'images' => $row['files'] ? explode(',', $row['files']) : null,
            'price' => $row['price'],
            'author' => [
                'name' => $row['name'],
                'email' => $row['email']
            ]
        );
    }
}