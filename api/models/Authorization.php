<?php

class Authorization
{
    // DB-Properties
    private $conn;
    private $table = 'session';

    // Auth-Properties
    public $id;
    public $token;

    // Constructor
    public function __construct($db)
    {
        $this->conn = $db;

        $this->id = $_SERVER['PHP_AUTH_USER'];
        $this->token = $_SERVER['PHP_AUTH_PW'];

    }

    // Init Authorization
    public function authorize()
    {
        $this->deleteSessions();

        // SQL-Query
        $query = 'SELECT * FROM ' . $this->table . ' 
                    WHERE User_ID = :userId
                    AND Token = :token
                    AND timestamp > (NOW() - INTERVAL 2 HOUR)';

        // Prepare statement
        $stmt = $this->conn->prepare($query);


        // Clean data
        $this->id = htmlspecialchars(strip_tags($this->id));
        $this->token = htmlspecialchars(strip_tags($this->token));

        // Bind ID
        $stmt->bindParam(':userId', $this->id);
        $stmt->bindParam(':token', $this->token);

        // Execute query
        if ($stmt->execute()) {
            $res = $stmt->fetchAll();

            if (count($res) == 1) {
                $sessionId = $res[0]['id'];

                $this->updateSession($sessionId);
            } else {
                exit(http_response_code(401));
            }
        }
    }

    public function updateSession($sessionID)
    {
        $query = "UPDATE session SET timestamp = now() WHERE ID = ?";

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Bind ID
        $stmt->bindParam(1, $sessionID);

        // Execute query
        $stmt->execute();
    }

    public function deleteSessions()
    {
        $query = " DELETE FROM session WHERE timestamp < (NOW() - INTERVAL 2 HOUR);";

        // Prepare statement
        $stmt = $this->conn->prepare($query);

        // Execute query
        $stmt->execute();
    }
}