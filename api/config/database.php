<?php

class Database
{
    // DB-Eigenschaften
    private $host = 'ddev-gardenrent-platform-db:3306';
    private $db_name = 'db';
    private $username = 'db';
    private $password = 'db';
    private $conn;

    // DB-Verbindung
    public function connect()
    {
        $this->conn = null;

        try {
            $this->conn = new PDO(
                'mysql:host=' . $this->host . ';dbname=' . $this->db_name,
                $this->username,
                $this->password,
                array(PDO::ATTR_PERSISTENT => true)
            );

            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Connection Error: ' . $e->getMessage();
        }

        return $this->conn;
    }
}