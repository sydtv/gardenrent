<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once './../../config/Database.php';
include_once './../../models/User.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate car object
$user = new User($db);

$user->email = $_POST["email"];
$user->name = $_POST["name"];
$user->password = password_hash($_POST["password"], PASSWORD_DEFAULT);

// Register user
if ($user->register()) {
    echo json_encode([
        'status' => 'success',
        'message' => 'Registrierung erfolgreich.'
    ]);
} else {
    echo json_encode([
        'status' => 'error',
        'message' => 'Registrierung fehlgeschlagen.'
    ]);
}