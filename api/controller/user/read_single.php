<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once './../../config/Database.php';
include_once './../../models/Authorization.php';
include_once './../../models/User.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

$authorization = new Authorization($db);
$authorization->authorize();

// Instantiate user object
$user = new User($db);

// Get ID
$user->id = isset($_POST['userID']) ? $_POST['userID'] : die();

// Get user
$user->read_single();

// User-Array
$user_arr = array(
    'id' => $user->id,
    'name' => $user->name,
    'email' => $user->email,
);

// Make JSON
print_r(json_encode($user_arr));