<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once './../../config/Database.php';
include_once './../../models/User.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate car object
$user = new User($db);

$user->email = $_POST["email"];
$user->password = $_POST["password"];

// Register user
if ($user->login()) {
    echo json_encode([
        'message' => 'Login erfolgreich.',
        'id' => $user->id,
        'token' => $user->token,
    ]);
} else {
    echo json_encode([
        'message' => 'Login failed.'
    ]);
}