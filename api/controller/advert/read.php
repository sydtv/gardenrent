<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once './../../config/Database.php';
include_once './../../models/Advert.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate advert object
$advert = new Advert($db);

// Advert-Query
$result = $advert->read();

// Row Count
$num = $result->rowCount();

// Check if fuels exist
if ($num > 0) {

    // Auto-Array
    $advert_arr = array();
    $advert_arr['data'] = array();

    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        extract($row);

        $advert_item = $advert->getAdvertItem($row);

        // Push to data-array
        array_push($advert_arr['data'], $advert_item);
    }

    // Make JSON
    echo json_encode($advert_arr);

} else {
    // Output if no fuels exist
    echo json_encode(
        array('message' => 'No adverts found')
    );
}