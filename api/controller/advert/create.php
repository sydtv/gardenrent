<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Access-Control-Allow-Origin,Access-Control-Allow-Methods,Content-Type,X-Requested-With");

include_once './../../config/Database.php';
include_once './../../models/Advert.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate advert object
$advert = new Advert($db);

$advert->title = $_POST['title'] ?? die();
$advert->description = $_POST['description'] ?? die();
$advert->address = $_POST['address'] ?? die();
$advert->zip = $_POST['zip'] ?? die();
$advert->city = $_POST['city'] ?? die();
$advert->price = $_POST['price'] ?? die();
$advert->authorId = $_POST['userId'] ?? die();
$advert->files = $_FILES['images'];
$advert->available = true;

// Create car
if ($advert->create()) {
    echo json_encode(
        array(
            'message' => 'Advert created',
            'success' => true
        )
    );
} else {
    echo json_encode(
        array(
            'message' => 'Advert not created',
            'success' => false
        )
    );
}