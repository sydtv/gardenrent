<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once './../../config/Database.php';
include_once './../../models/Advert.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate advert object
$advert = new Advert($db);

// Get ID
$advert->id = isset($_GET['advertId']) ? $_GET['advertId'] : die();

// Get advert
$result = $advert->read_single();

// Row Count
$num = $result->rowCount();

// Check if fuels exist
if ($num > 0) {

    // Auto-Array
    $advert_arr = array();
    $advert_arr['data'] = array();

    $row = $result->fetch(PDO::FETCH_ASSOC);

    $advert_item = $advert->getAdvertItem($row);

    // Make JSON
    echo json_encode($advert_item);
} else {
    // Output if no fuels exist
    echo json_encode(
        array('message' => 'No advert with this id found found')
    );
}