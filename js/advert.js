getAdvert();

function getAdvert() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const advertId = urlParams.get('id');

    if (advertId) {
        fetch(`/api/controller/advert/read_single.php?advertId=${advertId}`,
            {
                method: "GET"
            })
            .then((res) => {
                if (res.status >= 200 && res.status < 300) {
                    return res.json();
                }
            })
            .then((data) => {
                if (data) {
                    const item = document.createElement('div');

                    const availableDot = document.createElement('div');
                    availableDot.innerHTML = data.available ? 'Angebot verfügbar' : 'Angebot vergeben';
                    availableDot.className = `text-sm mb-2 font-bold p-2 text-center rounded text-white ${data.available ? 'bg-green-500' : 'bg-red-500'}`;
                    item.appendChild(availableDot);


                    const title = document.createElement('div');
                    title.innerHTML = data.title;
                    title.className = 'text-xl mb-2 font-bold';
                    item.appendChild(title);

                    const description = document.createElement('p');
                    description.innerHTML = data.description;
                    description.className = 'mb-2';
                    item.appendChild(description);

                    const address = document.createElement('p');
                    address.innerHTML = data.address;
                    address.className = 'mb-0';
                    item.appendChild(address);

                    const zipCity = document.createElement('p');
                    zipCity.innerHTML = `${data.zip} ${data.city}`;
                    zipCity.className = 'mb-2';
                    item.appendChild(zipCity);

                    if (data.images) {
                        data.images.forEach((imageFile) => {
                            const container = document.createElement('div');
                            container.className = 'swiper-slide';

                            const image = new Image();
                            image.src = imageFile;
                            image.className = 'object-cover object-center h-96 w-full';

                            container.appendChild(image);

                            document.querySelector('.swiper-wrapper').appendChild(container);
                        });

                        const swiper = new Swiper('.swiper', {
                            // Optional parameters
                            loop: true,

                            // If we need pagination
                            pagination: {
                                el: '.swiper-pagination',
                            },

                            // Navigation arrows
                            navigation: {
                                nextEl: '.swiper-button-next',
                                prevEl: '.swiper-button-prev',
                            },

                            // And if we need scrollbar
                            scrollbar: {
                                el: '.swiper-scrollbar',
                            },
                        });
                    }

                    if(data.available) {
                        const ctaButton = document.createElement('a');
                        ctaButton.className = 'mb-4 p-4 bg-blue-500 text-white text-center rounded block';
                        ctaButton.innerHTML = `Jetzt ${data.author.name} kontaktieren!`;
                        ctaButton.href = `mailto:${data.author.email}`;
                        item.appendChild(ctaButton);
                    }

                    document.querySelector('div[data-advert-container="advert"]').appendChild(item);
                }
            })
    }
}