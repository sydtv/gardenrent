getAdverts();

function getAdverts() {
    fetch("/api/controller/advert/read.php",
        {
            method: "get"
        })
        .then((res) => {
            if (res.status >= 200 && res.status < 300) {
                return res.json();
            }
        })
        .then((data) => {
            if (data.data) {
                data.data.forEach((advert) => {
                    const item = document.createElement('a');
                    item.className = 'p-4 rounded border border-black block';
                    item.href = `/advert.html?id=${advert.id}`;

                    const availableDot = document.createElement('div');
                    availableDot.innerHTML = advert.available ? 'Angebot verfügbar' : 'Angebot vergeben';
                    availableDot.className = `text-sm mb-2 font-bold p-2 text-center rounded text-white ${advert.available ? 'bg-green-500' : 'bg-red-500'}`;
                    item.appendChild(availableDot);


                    const title = document.createElement('div');
                    title.innerHTML = advert.title;
                    title.className = 'text-xl mb-2 font-bold';
                    item.appendChild(title);

                    const description = document.createElement('p');
                    description.innerHTML = advert.description.substr(0, 150) + '...';
                    description.className = 'mb-2';
                    item.appendChild(description);

                    const address = document.createElement('p');
                    address.innerHTML = advert.address;
                    address.className = 'mb-0';
                    item.appendChild(address);

                    const zipCity = document.createElement('p');
                    zipCity.innerHTML = `${advert.zip} ${advert.city}`;
                    zipCity.className = 'mb-2';
                    item.appendChild(zipCity);

                    if (advert.images) {
                        const image = new Image();
                        image.src = advert.images[0];
                        image.className = 'object-cover object-center h-48 w-full'
                        item.appendChild(image);
                    }

                    document.querySelector('div[data-advert-container="adverts"]').appendChild(item);
                });
            }
        })
}