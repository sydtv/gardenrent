let registerForm = document.querySelector("#register-form");

if (registerForm) {
    registerForm.addEventListener('submit', (e) => {
        e.preventDefault();
        register();
    });
}

function register() {
    let formData = new FormData(registerForm);

    fetch("/api/controller/user/register.php",
        {
            body: formData,
            method: "post",
        })
        .then((res) => res.json())
        .then((data) => {
            if (data.status === 'success') {
                window.location.href = "/login.html";
            } else {
                document.querySelector('#message').innerHTML = data.message;
            }
        })
}