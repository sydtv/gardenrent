const loginForm = document.querySelector('#login-form');

if(loginForm) {
    loginForm.addEventListener('submit', (e) => {
        e.preventDefault();
        login();
    });
}

function login() {
    let formData = new FormData(loginForm);

    fetch("/api/controller/user/login.php",
        {
            body: formData,
            method: "post",
        })
        .then(res => res.json())
        .then((data) => {
            document.querySelector('#message').innerHTML = data.message;

            localStorage.setItem("userID", data.id);
            localStorage.setItem("token", data.token);

            if (data.id && data.token) {
                window.location.href = "/";
            }
        })
}