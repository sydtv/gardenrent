const burgerButton = document.querySelector('#burger-button');
const mobileMenu = document.querySelector('#mobile-menu')
if(burgerButton && mobileMenu) {
    burgerButton.addEventListener('click', () => {
        if(mobileMenu.classList.contains('block')) {
            mobileMenu.className = 'hidden';
        } else {
            mobileMenu.className = 'block md:hidden';
        }
    });
}