getUser();

function getUser() {
    let userID = localStorage.getItem('userID');
    let token = localStorage.getItem('token');

    let formData = new FormData();
    formData.append('userID', userID);

    fetch("/api/controller/user/read_single.php",
        {
            body: formData,
            method: "post",
            headers: {
                'Authorization': 'Basic ' + btoa(userID + ':' + token),
            }
        })

        .then((res) => {

            // falls die Sitzung nicht abgelaufen ist, verarbeite die JSON antwort
            if (res.status >= 200 && res.status < 300) {
                return res.json();
            } else {
                alert('Deine Sitzung ist abgelaufen. Du wirst auf die Login-Seite weitergeleitet.');
                window.location = "/login.html"
                throw new Error('Deine Sitzung ist abgelaufen. Du wirst auf die Login-Seite weitergeleitet.');
            }

        })
        .then((data) => {
            document.getElementById("username").innerHTML = data.name;
            document.getElementById("user-email").innerHTML = data.email;
        })
}