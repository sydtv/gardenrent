const newAdvertForm = document.querySelector('#new-advert-form');

if (newAdvertForm) {
    newAdvertForm.addEventListener('submit', (e) => {
        e.preventDefault();
        createAdvert();
    });
}

function createAdvert() {
    let userID = localStorage.getItem('userID');

    let formData = new FormData(newAdvertForm);
    formData.append('userId', userID);

    fetch("/api/controller/advert/create.php",
        {
            body: formData,
            method: "post",
        })
        .then(res => res.json())
        .then((data) => {
            if (data.success) {
                alert('Ihr Inserat wurde erfolgreich erstellt!');
                window.location = '/';
            } else {
                alert('Es ist ein Fehler aufgetreten!');
            }
        })
}