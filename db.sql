DROP DATABASE IF EXISTS `db`;
CREATE DATABASE `db`;
USE `db`;

SET NAMES utf8mb4;
SET character_set_client = utf8mb4;

CREATE TABLE `user`
(
    id       int          NOT NULL,
    name     varchar(100) NOT NULL,
    email    varchar(100) NOT NULL,
    password varchar(200) NOT NULL,
    PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `session`
(
    id        int         NOT NULL AUTO_INCREMENT,
    user_id   int         NOT NULL,
    token     varchar(42) NOT NULL,
    timestamp timestamp   NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES user (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `advert`
(
    id          int          NOT NULL AUTO_INCREMENT,
    author      int          NOT NULL,
    title       varchar(100) NOT NULL,
    description TEXT         NOT NULL,
    price       varchar(100) NOT NULL,
    address     TEXT         NOT NULL,
    zip         TEXT         NOT NULL,
    city        TEXT         NOT NULL,
    available   boolean      NOT NULL,
    created_at  timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (author) REFERENCES user (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci
  ROW_FORMAT = DYNAMIC;

CREATE TABLE `advert_files`
(
    id        int          NOT NULL AUTO_INCREMENT,
    location  varchar(200) NOT NULL,
    advert_id int          NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (advert_id) REFERENCES advert (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci
  ROW_FORMAT = DYNAMIC;

INSERT INTO `user` (id, name, email, password)
VALUES (16, 'Nick', 'nick@nick.ch', '$2y$10$/E8Lc0FoICrSlKl.cBRgUOvAR.mJpwI3CN93QUiA.m9wCAc1uM8j.'),
       (18, 'Eva', 'eva.ang@gmail.com', '$2y$10$Qrtxsn2fv2LbiC/R5oUEq.BOy3SFxpMMM1xWepjd5gi2lDuxGtLE6'),
       (19, 'Nick Schneeberger', 'hallo@nickschnee.ch', '$2y$10$jrZgpHNx0GF0fBq7ALbEmeVE0TAEdgNY6zJuwm4yd7i01RD6Fp0yy'),
       (20, 'Blub', 'blub@blub.ch', '$2y$10$V8BNTbmfaz.4B2Y8MYWjDOftzDUhJAEMv/sqqWZ3YS5BwHUnj7Jxa'),
       (23, 'Eva Öpfuschmatz', 'eva@?pfuschmatz.ch', '$2y$10$09pR6A8/qGiMCekoYucDke1FJL6ZCrY3HzLhsBTwk09GtsqD44LWa'),
       (24, 'Samuel', 'samuel@codecrush.ch', '$2y$10$S02/dbn7KaSdfPFNgRRw0es.TaQCVKqZ5UNI2zAfJWEQg2bgI9FC.'),
       (26, 'Nick', 'nickjonas@schneeberger.ch', '$2y$10$mzCD8fNdH20QaUgkiFb3zOPuYr7.Sk.W0C4JxofoABTJXIEnKxYha'),
       (27, 'blub', 'blub@blub.com', '$2y$10$NVTnYkrjTv1MGazU5ZxpMeu1FnL5NurYzXCzRclQuCQY7RsnZayke'),
       (25, 'sydtv', 'contact@yves-schlimpert.ch', '$2y$10$PtxBGBUEPr1qD3yiNDFKUuuadJjSxFf/oUD.jXexoxW6ObjTpZw9C');

INSERT INTO `advert` (id, author, title, description, price, available, address, zip, city)
VALUES (1, 16, 'I dont like it here Morty. I cant abide bureaucracy.', 'If Ive learned one thing, its that before you get anywhere in life, you gotta stop listening to yourself. Its fine, everythings is fine. theres an infinite number of realities Morty, and in a few dozens of those i got lucky and turned everything back to normal. I love morty and i hope morty loves me. I want to wrap my arms around him and feel him deep inside me. Nothing you do matters, your existence is a lie!
Man, that guy is the Red Grin Grumbold of pretending he knows whats going on. Oh you agree huh? You like that Red Grin Grumbold reference? Well guess what, I made him up. You really are your fathers children. Think for yourselves, dont be sheep. Ill tell you how I feel about school, Jerry. Its a waste of time. Bunch of people runnin around bumpin into each other, got a guy up front says, 2 + 2, and the people in the back say, 4. Then the bell rings and they give you a carton of milk and a piece of paper that says you can go take a dump or somethin. I mean, its not a place for smart people, Jerry. I know thats not a popular opinion, but thats my two cents on the issue. This isnt Game of Thrones, Morty. Jesus Christ! Did the boomy-booms blow up all your wordy-word books?
I took your family? Who do you think had taken more from them when you shot 2 Don''t break an arm jerking yourself off Morty. And now, human music. boop beep boop... boop beep boop... Existence is pain to a meeseeks Jerry, and we will do anything to alleviate that pain.',
        '50 CHF VB', true, 'Hauptstrasse 12', '9000', 'St. Gallen'),
       (2, 18, 'Did you get those seeds all the way up your butt?', 'If Ive learned one thing, its that before you get anywhere in life, you gotta stop listening to yourself. Its fine, everythings is fine. theres an infinite number of realities Morty, and in a few dozens of those i got lucky and turned everything back to normal. I love morty and i hope morty loves me. I want to wrap my arms around him and feel him deep inside me. Nothing you do matters, your existence is a lie!
Man, that guy is the Red Grin Grumbold of pretending he knows whats going on. Oh you agree huh? You like that Red Grin Grumbold reference? Well guess what, I made him up. You really are your fathers children. Think for yourselves, dont be sheep. Ill tell you how I feel about school, Jerry. Its a waste of time. Bunch of people runnin around bumpin into each other, got a guy up front says, 2 + 2, and the people in the back say, 4. Then the bell rings and they give you a carton of milk and a piece of paper that says you can go take a dump or somethin. I mean, its not a place for smart people, Jerry. I know thats not a popular opinion, but thats my two cents on the issue. This isnt Game of Thrones, Morty. Jesus Christ! Did the boomy-booms blow up all your wordy-word books?
I took your family? Who do you think had taken more from them when you shot 2 Don''t break an arm jerking yourself off Morty. And now, human music. boop beep boop... boop beep boop... Existence is pain to a meeseeks Jerry, and we will do anything to alleviate that pain.',
        '80 CHF', false, 'Hauptstrasse 12', '9000', 'St. Gallen'),
       (3, 19, 'You know what shy pooping is, Rick?', 'If Ive learned one thing, its that before you get anywhere in life, you gotta stop listening to yourself. Its fine, everythings is fine. theres an infinite number of realities Morty, and in a few dozens of those i got lucky and turned everything back to normal. I love morty and i hope morty loves me. I want to wrap my arms around him and feel him deep inside me. Nothing you do matters, your existence is a lie!
Man, that guy is the Red Grin Grumbold of pretending he knows whats going on. Oh you agree huh? You like that Red Grin Grumbold reference? Well guess what, I made him up. You really are your fathers children. Think for yourselves, dont be sheep. Ill tell you how I feel about school, Jerry. Its a waste of time. Bunch of people runnin around bumpin into each other, got a guy up front says, 2 + 2, and the people in the back say, 4. Then the bell rings and they give you a carton of milk and a piece of paper that says you can go take a dump or somethin. I mean, its not a place for smart people, Jerry. I know thats not a popular opinion, but thats my two cents on the issue. This isnt Game of Thrones, Morty. Jesus Christ! Did the boomy-booms blow up all your wordy-word books?
I took your family? Who do you think had taken more from them when you shot 2 Don''t break an arm jerking yourself off Morty. And now, human music. boop beep boop... boop beep boop... Existence is pain to a meeseeks Jerry, and we will do anything to alleviate that pain.',
        '30 CHF', true, 'Hauptstrasse 12', '9000', 'St. Gallen'),
       (5, 23, 'I dont like being told where to go and what to do. ', 'If Ive learned one thing, its that before you get anywhere in life, you gotta stop listening to yourself. Its fine, everythings is fine. theres an infinite number of realities Morty, and in a few dozens of those i got lucky and turned everything back to normal. I love morty and i hope morty loves me. I want to wrap my arms around him and feel him deep inside me. Nothing you do matters, your existence is a lie!
Man, that guy is the Red Grin Grumbold of pretending he knows whats going on. Oh you agree huh? You like that Red Grin Grumbold reference? Well guess what, I made him up. You really are your fathers children. Think for yourselves, dont be sheep. Ill tell you how I feel about school, Jerry. Its a waste of time. Bunch of people runnin around bumpin into each other, got a guy up front says, 2 + 2, and the people in the back say, 4. Then the bell rings and they give you a carton of milk and a piece of paper that says you can go take a dump or somethin. I mean, its not a place for smart people, Jerry. I know thats not a popular opinion, but thats my two cents on the issue. This isnt Game of Thrones, Morty. Jesus Christ! Did the boomy-booms blow up all your wordy-word books?
I took your family? Who do you think had taken more from them when you shot 2 Don''t break an arm jerking yourself off Morty. And now, human music. boop beep boop... boop beep boop... Existence is pain to a meeseeks Jerry, and we will do anything to alleviate that pain.',
        '25 CHF', false, 'Hauptstrasse 12', '9000', 'St. Gallen'),
       (4, 24, 'Prepare to be emancipated from your own inferior genes!', 'If Ive learned one thing, its that before you get anywhere in life, you gotta stop listening to yourself. Its fine, everythings is fine. theres an infinite number of realities Morty, and in a few dozens of those i got lucky and turned everything back to normal. I love morty and i hope morty loves me. I want to wrap my arms around him and feel him deep inside me. Nothing you do matters, your existence is a lie!
Man, that guy is the Red Grin Grumbold of pretending he knows whats going on. Oh you agree huh? You like that Red Grin Grumbold reference? Well guess what, I made him up. You really are your fathers children. Think for yourselves, dont be sheep. Ill tell you how I feel about school, Jerry. Its a waste of time. Bunch of people runnin around bumpin into each other, got a guy up front says, 2 + 2, and the people in the back say, 4. Then the bell rings and they give you a carton of milk and a piece of paper that says you can go take a dump or somethin. I mean, its not a place for smart people, Jerry. I know thats not a popular opinion, but thats my two cents on the issue. This isnt Game of Thrones, Morty. Jesus Christ! Did the boomy-booms blow up all your wordy-word books?
I took your family? Who do you think had taken more from them when you shot 2 Don''t break an arm jerking yourself off Morty. And now, human music. boop beep boop... boop beep boop... Existence is pain to a meeseeks Jerry, and we will do anything to alleviate that pain.',
        '80 CHF VB', true, 'Hauptstrasse 12', '9000', 'St. Gallen'),
       (6, 26, 'Just keep shooting, Morty! You have no idea what prison is like here! ', 'If Ive learned one thing, its that before you get anywhere in life, you gotta stop listening to yourself. Its fine, everythings is fine. theres an infinite number of realities Morty, and in a few dozens of those i got lucky and turned everything back to normal. I love morty and i hope morty loves me. I want to wrap my arms around him and feel him deep inside me. Nothing you do matters, your existence is a lie!
Man, that guy is the Red Grin Grumbold of pretending he knows whats going on. Oh you agree huh? You like that Red Grin Grumbold reference? Well guess what, I made him up. You really are your fathers children. Think for yourselves, dont be sheep. Ill tell you how I feel about school, Jerry. Its a waste of time. Bunch of people runnin around bumpin into each other, got a guy up front says, 2 + 2, and the people in the back say, 4. Then the bell rings and they give you a carton of milk and a piece of paper that says you can go take a dump or somethin. I mean, its not a place for smart people, Jerry. I know thats not a popular opinion, but thats my two cents on the issue. This isnt Game of Thrones, Morty. Jesus Christ! Did the boomy-booms blow up all your wordy-word books?
I took your family? Who do you think had taken more from them when you shot 2 Don''t break an arm jerking yourself off Morty. And now, human music. boop beep boop... boop beep boop... Existence is pain to a meeseeks Jerry, and we will do anything to alleviate that pain.',
        '50 CHF', true, 'Hauptstrasse 12', '9000', 'St. Gallen'),
       (7, 27, 'You know what a vole is, Morty?', 'If Ive learned one thing, its that before you get anywhere in life, you gotta stop listening to yourself. Its fine, everythings is fine. theres an infinite number of realities Morty, and in a few dozens of those i got lucky and turned everything back to normal. I love morty and i hope morty loves me. I want to wrap my arms around him and feel him deep inside me. Nothing you do matters, your existence is a lie!
Man, that guy is the Red Grin Grumbold of pretending he knows whats going on. Oh you agree huh? You like that Red Grin Grumbold reference? Well guess what, I made him up. You really are your fathers children. Think for yourselves, dont be sheep. Ill tell you how I feel about school, Jerry. Its a waste of time. Bunch of people runnin around bumpin into each other, got a guy up front says, 2 + 2, and the people in the back say, 4. Then the bell rings and they give you a carton of milk and a piece of paper that says you can go take a dump or somethin. I mean, its not a place for smart people, Jerry. I know thats not a popular opinion, but thats my two cents on the issue. This isnt Game of Thrones, Morty. Jesus Christ! Did the boomy-booms blow up all your wordy-word books?
I took your family? Who do you think had taken more from them when you shot 2 Don''t break an arm jerking yourself off Morty. And now, human music. boop beep boop... boop beep boop... Existence is pain to a meeseeks Jerry, and we will do anything to alleviate that pain.',
        '50 CHF VB', false, 'Hauptstrasse 12', '9000', 'St. Gallen');

INSERT INTO `advert_files` (id, advert_id, location)
VALUES (16, 1,
        'https://images.unsplash.com/photo-1528607950896-30d92379b592?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1548&q=80'),
       (18, 2,
        'https://images.unsplash.com/photo-1534614310503-71f6d476047d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80'),
       (19, 1,
        'https://images.unsplash.com/photo-1532509774891-141d37f25ae9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80'),
       (20, 3,
        'https://images.unsplash.com/photo-1590682680695-43b964a3ae17?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1400&q=80'),
       (23, 4,
        'https://images.unsplash.com/photo-1532509774891-141d37f25ae9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80'),
       (24, 5,
        'https://images.unsplash.com/photo-1528607950896-30d92379b592?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1548&q=80'),
       (26, 3,
        'https://images.unsplash.com/photo-1590682680695-43b964a3ae17?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1400&q=80'),
       (27, 7,
        'https://images.unsplash.com/photo-1532509774891-141d37f25ae9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80'),
       (29, 2,
        'https://images.unsplash.com/photo-1532509774891-141d37f25ae9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80');
